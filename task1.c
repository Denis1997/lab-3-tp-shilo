//Simple task of counting your age in days
#include <stdio.h>

int main()
{
	int n;
	printf("Enter your age(years): ");
	scanf("%d", &n);				//reading your age
	printf("Your age(days): %d\n", n * 365);	//printing your age multipled by 365
	return 0;
}
