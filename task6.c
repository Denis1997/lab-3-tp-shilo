#include <stdio.h>
#include <string.h>

#define TRUE 1
#define FALSE 0

struct place
{
    int placeID;	//ID of the place
    int isFree;		//has value TRUE if place is free and FALSE otherwise	
    char fstName[30];	//first name of a passenger
    char secName[30];	//second name of a passenger
};

void showMenu();

int freePlacesNumber = 12;
struct place places[12];

void LoadInfo();	//load info from data file
void UploadInfo();	//upload info in data file

int main()
{
    int i;
    char command = 'a';
    for (i = 0; i < 12; i++)
    {
        places[i].isFree = TRUE;
        places[i].placeID = i + 1;
    }
    LoadInfo();
    while (command != 'f')
    {
        showMenu();
        scanf("%c", &command);
        getchar();
        switch(command)
        {
            case 'a':
                printf("Number of free places: %d\n", freePlacesNumber);
                break;
            case 'b':
                printf("List of free places:\n");
                for (i = 0; i < 12; i++)
                    if (places[i].isFree == TRUE)
                        printf("\t%d\n", places[i].placeID);
                break;
            case 'c':
                printf("List of booked places:\n");
                for (i = 0; i < 12; i++)
                    if (places[i].isFree == FALSE)
                        printf("\tplace %d:\n\t\t%s\n\t\t%s\n", i + 1, places[i].fstName, places[i].secName);
                break;
            case 'd':
            {
                char line[30];
                int plNum;
                if (freePlacesNumber == 0)
                {
                    printf("There are no free places.");
                    break;
                }
                printf("Enter place num, or (-1) to break: ");
                scanf("%d", &plNum);
                while (plNum != -1)
                {
                    if (plNum <= 0 || plNum > 12)
                        printf("Index out of range. Try again: ");
                    else
                        if (places[plNum - 1].isFree == FALSE)
                            printf("Place if alreay booked. Try again: ");
                        else
                            break;
                    scanf("%d", &plNum);
                }
                if (plNum == -1)
                {
                    getchar();
                    break;
                }
                printf("Enter first name or (!) to break: ");
                scanf("%s", line);
                if (line[0] == '!')
                {
                    getchar();
                    break;
                }
                else
                    strcpy(places[plNum - 1].fstName, line);
                
                printf("Enter second name or (!) to break: ");
                scanf("%s", line);
                if (line[0] == '!')
                {
                    getchar();
                    break;
                }
                else
                    strcpy(places[plNum - 1].secName, line);
                places[plNum - 1].isFree = FALSE;
                freePlacesNumber--;
                getchar();
            }
                break;
            case 'e':
            {
                int plNum;
                printf("Enter place num, or (-1) to break: ");
                scanf("%d", &plNum);
                while (plNum != -1)
                {
                    if (plNum <= 0 || plNum > 12)
                        printf("Index out of range. Try again: ");
                    else
                        if (places[plNum - 1].isFree == TRUE)
                            printf("Place if alreay free. Try again: ");
                        else
                            break;
                    scanf("%d", &plNum);
                }
                if (plNum == -1)
                {
                    getchar();
                    break;
                }
                places[plNum - 1].isFree = TRUE;
                freePlacesNumber++;
                getchar();
            }
                break;
            case 'f':
                UploadInfo();
                break; 
            default:
                break;
        }
    }
    return 0;
}

void showMenu()
{
    printf("------MENU------\n");
    printf("\ta)Show number of free places\n");
    printf("\tb)Show list of free places\n");
    printf("\tc)Show list of booked places\n");
    printf("\td)Book place\n");
    printf("\te)Free place\n");
    printf("\tf)Exit\n");
    printf("Enter command (a, b, c...): ");
}

void LoadInfo()
{
    FILE* inFile = fopen("in.info", "rb");	//open info file
    int q;					
    if (inFile == NULL)				//if can't open -> return
        return;
    freePlacesNumber = 0;
    for (q = 0; q < 12 ; q++)
    {
        fread(&places[q], sizeof(struct place), 1, inFile);	//getting info
        if (places[q].isFree == TRUE)
            freePlacesNumber++;				//counting number of 
    }							//free places
    fclose(inFile);
}

void UploadInfo()
{
    FILE* outFile = fopen("in.info", "wb");	//open info file
    int q;
    if (outFile == NULL)			//if can't open file -> return
        return;
    for (q = 0; q < 12 ; q++)
        fwrite(&places[q], sizeof(struct place), 1, outFile); //writting info 
    fclose(outFile);					      //in the file	
}
